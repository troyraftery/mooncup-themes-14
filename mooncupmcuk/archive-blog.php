<?php
/**
 * Mooncup Main template for displaying Archives
 *
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 */

get_header(); ?>

	<section class="blog page-content primary" role="main">
		<div class="container_full">
			<?php $my_query = new WP_Query( array('pagename' => 'mooncup-blog', 'showposts' => '999')); ?>
			<?php if ($my_query->have_posts()): ?>
			<?php while ( $my_query->have_posts() ) : $my_query->the_post(); ?>	
			<?php

			/*
			*  Loop through post objects (assuming this is a multi-select field) ( setup postdata )
			*  Using this method, you can use all the normal WP functions as the $post object is temporarily initialized within the loop
			*  Read more: http://codex.wordpress.org/Template_Tags/get_posts#Reset_after_Postlists_with_offset
			*/

			$post_objects = get_field('post_objects');

			if( $post_objects ): ?>
			
			    <ul class="blog-slider">
			    <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
			        <?php setup_postdata($post); ?>
			        <li>
			        	<div class="blog-slide">
			        		<div class="blog-slide--image">
					        	<?php if (has_post_thumbnail( $post->ID ) ): ?>
								<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
					        		<div class = "content post-featured-image image-cover" style="background-image:url('<?php echo $image[0]; ?>');"></div>
						    	<?php endif; ?>
					        </div>
			        		<div class="blog-slide--content">
					            <a href="<?php the_permalink(); ?>"><h1><?php the_title(); ?></h1></a>
					            <span><?php the_excerpt()?></span>
					            <a href="<?php the_permalink(); ?>" class="btn-black">Read more</a>
					        </div>
					        
			        	</div>
			        </li>
			    <?php endforeach; ?>
			    </ul>
			
			    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			<?php endif;

		

			?>
			<?php endwhile;?>
			<?php endif;?>
			<?php wp_reset_query(); ?>
		</div>

		<div class="container_boxed content_band--small no-pad-top">
			<div class="container__full blog-utility">
				
				<div class="blog-utility--item breadcrumbs">
					<?php if ( function_exists('yoast_breadcrumb') ) 
					{yoast_breadcrumb('<span id="breadcrumbs">','</span>');} ?>
				</div>
				<div class="align-right">
					<div class="blog-utility--item search">
						<?php include (TEMPLATEPATH . '/searchform.php'); ?>
					</div>

					<div class="blog-utility--item social">
						<?php $my_query = new WP_Query( array('pagename' => 'mooncup-blog', 'showposts' => '999')); ?>
						<?php if ($my_query->have_posts()): ?>
						<?php while ( $my_query->have_posts() ) : $my_query->the_post(); ?>	
						<ul>
							<li class="social-icon"><a href="<?php the_field('facebook_link'); ?>" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
							<li class="social-icon"><a href="<?php the_field('twitter_link'); ?>" target="_blank"><i class="fa fa-twitter-square"></i></a></li>
							<li class="social-icon"><a href="<?php the_field('google_link'); ?>" target="_blank"><i class="fa fa-google-plus-square"></i></a></li>
							<li class="social-icon"><a href="<?php the_field('youtube_link'); ?>" target="_blank"><i class="fa fa-youtube-square"></i></a></li>
							<li class="social-icon"><a href="<?php the_field('pinterest_link'); ?>" target="_blank"><i class="fa fa-pinterest-square"></i></a></li>
							<li class="social-icon"><a href="<?php the_field('instagram_link'); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
						</ul>
						<?php endwhile;?>
						<?php endif;?>
						<?php wp_reset_query(); ?>
					</div>
				</div>

			</div>
			
				<ul class="nav--blog-category">
				    <?php wp_list_categories( array(
				    	'taxonomy' => 'blogs',
				        'orderby' => 'name',
				        'title_li' => '',
				        'exclude' => '129'
				    ) ); ?> 
				</ul>
		
	    
		    <section class="blog-LP three--col-grid">
			<?php

				if ( have_posts() ) : ?>

						<?php

						if ( is_category() || is_tag() || is_tax() ):
							$term_description = term_description();
							if ( ! empty( $term_description ) ) : ?>

								<div class="archive-description"><?php
									echo $term_description; ?>
								</div><?php

							endif;
						endif;

						if ( is_author() && get_the_author_meta( 'description' ) ) : ?>

							<div class="archive-description">
								<?php the_author_meta( 'description' ); ?>
							</div><?php

						endif;?>

					<div class="container_full">
						<?php

						while ( have_posts() ) : the_post();

							get_template_part( 'loop', 'blog' );

						endwhile;

					else :

						get_template_part( 'loop', 'empty' );

					endif; ?>
					</div>				          
					

					<div class="pagination">

						<?php get_template_part( 'template-part', 'pagination' ); ?>

					</div>
				</section>


		</div>
	</section>

<?php get_footer(); ?>