<?php
/**
 * Mooncup Main template for displaying custom search
 *
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 */
?>

<form action="/" method="get" class="searchbox">
	<label for="search">Search in <?php echo home_url( '/' ); ?></label>
	<input type="text" placeholder="Search" name="s" id="search" value="<?php the_search_query(); ?>" />
</form>