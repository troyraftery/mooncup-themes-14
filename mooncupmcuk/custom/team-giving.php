<?php
/**
 *
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 * Template Name: Team Giving Main
 */

get_header(); ?>
<section class="single-col page-content primary" role="main">
		
	        <article class="container_full splash-content-block">
	        	<div class = "splash-image-narrow splash-image_generic image_fullwidth" style="background-image:url('<?php the_field('splash_image'); ?>');">
		        	<div class="splash-content-overlay splash-header text-reverse">
		        		<div class="container_full">
			        	<?php the_field('splash_content'); ?>
			        	</div>
		        	</div>
		        </div>
		    </article>


	        <article class="container_full content_band">
	        	<div class="container_boxed--narrow">
	        		<?php the_field('1col_content_area');?>
	        	</div>
	        </article>
			
			<div class="slider-horizontal">
			<?php

			// check if the repeater field has rows of data
			if( have_rows('team_giving_item') ):?>
			

			    <?php while ( have_rows('team_giving_item') ) : the_row();?>
				
				<div class="team-giving-item container--lined">	        	
					<div class="team-giving-item__content center">
						<h3>
							<?php
							the_sub_field('team_giving_title');
							?>
						</h3>
						<?php
							the_sub_field('team_giving_content');
						?>
					</div>
				</div>
						       
			    <?php endwhile;?>
			</div>
			<?php 

			else :

			    // no rows found

			endif;

			?>

			<aside class="page-outro container_boxed content_band--lined">
	        	<div class="container_boxed--narrow content_band--small">
	        		<?php the_field('footer_area');?>
	        	</div>
	        </aside>
	
</section>

<?php get_footer(); ?>
