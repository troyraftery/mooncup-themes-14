<?php
/**
 *
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 * Template Name: Three Column with Splash Image
 */

get_header(); ?>
<section class="three-column page-content primary" role="main">
		
	        <div class="container_full splash-content-block">
	        	<div class = "splash-image-narrow splash-image_generic image_fullwidth" style="background-image:url('<?php the_field('splash_image'); ?>');">
		        	<div class="splash-content-overlay splash-header text-reverse">
		        		<div class="container_full">
			        	<?php the_field('splash_content'); ?>
			        	</div>
		        	</div>
		        </div>
		    </div>

		    <article class="container_full content_band">
	        	<div class="container_boxed--narrow">
	        			<?php the_field('intro_content_area');?>
	        	</div>
	        </article>

	        <article class="container_boxed container__3col">
	        	<div class="container__inner">
	        		<?php the_field('left_content_area');?>
	        	</div>
	        	<div class="container__inner">
	        		<?php the_field('middle_content_area');?>
	        	</div>
	        	<div class="container__inner">
	        		<?php the_field('right_content_area');?>
	        	</div>
	        </article>

	        <aside class="page-outro container_boxed content_band--lined">
	        	<div class="container_boxed--narrow content_band--small">
	        		<?php the_field('outro_content_area');?>
	        	</div>
	        </aside>
	
</section>

<?php get_footer(); ?>
