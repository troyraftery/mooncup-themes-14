<?php
/**
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 * Template Name: Country Select
 */

get_header();
?>
<section class="single-col page-content primary" role="main">

	        <article class="container_full splash-content-block">
	        	<div class = "splash-image-narrow splash-image_generic image_fullwidth" style="background-image:url('<?php the_field('splash_image'); ?>');">
		        	<div class="splash-content-overlay splash-header text-reverse">
		        		<div class="container_full">
			        	<?php the_field('splash_content'); ?>
			        	</div>
		        	</div>
		        </div>
		    </article>

	        	<article class="container_full content_band">
	        		<div class="container_boxed--narrow form-standard">
	        		<?php the_field('1col_content_area');?>
						<div>
							<h2><?php _e('Please select your country. If your country is not on the list, please select other','mooncupmain');?></h2>
							<?php
							$country = new WC_Countries;
							$countries = $country->get_allowed_countries();
							$all_countries = $country->get_countries();
							?>
							<!--
							<form method="GET" action="<?php echo get_permalink(icl_object_id(3545,'page',false));?>" >
								<p>
								<select id="location" name="geocode"  class="form-standard" onChange="jumpMenu('parent',this,0)" >
									<?php
									if(isset($_GET['geocode'])):
										$_COOKIE['location'] = $_GET['geocode'];
									endif;
									if($_COOKIE['location'] == 'GB'):
									   echo '<option value="GB" selected>';
										_e('United Kingdom (UK)','woocommerce');
										echo '</option>';
        								echo '<option value="US">';
										_e('United States (US)','woocommerce');
										echo '</option>';
        								echo '<option value="AU">';
										_e('Australia','woocommerce');
										echo '</option>';
									elseif($_COOKIE['location'] == 'US'):
										echo '<option value="GB">';
										_e('United Kingdom (UK)','woocommerce');
										echo '</option>';
										echo '<option value="US" selected>';
										_e('United States (US)','woocommerce');
										echo '</option>';
										echo '<option value="AU">';
										_e('Australia','woocommerce');
										echo '</option>';
									elseif($_COOKIE['location'] == 'AU'):
										echo '<option value="GB">';
										_e('United Kingdom (UK)','woocommerce');
										echo '</option>';
										echo '<option value="US">';
										_e('United States (US)','woocommerce');
										echo '</option>';
										echo '<option value="AU" selected>';
										_e('Australia','woocommerce');
										echo '</option>';
									else:
										echo '<option value="GB">';
										_e('United Kingdom (UK)','woocommerce');
										echo '</option>';
										echo '<option value="US">';
										_e('United States (US)','woocommerce');
										echo '</option>';
										echo '<option value="AU">';
										_e('Australia','woocommerce');
										echo '</option>';
    								endif;
    								echo '<option value="" disabled="disabled">--------------</option>';
    								foreach($countries as $key =>$value) {
    								    if($key == 'GB' || $key == 'AU' || $key == 'US' || $key == 'OO'):

    									elseif($key == $_COOKIE['location']):
    										echo '<option value="'.$key.'" selected>'.$value.'</option>';
    									else:
    							             echo '<option value="'.$key.'">'.$value.'</option>';
    									endif;
    							    }
    								echo '<option value="OO">';
									_e('Other','woocommerce');
									echo '</option>';



									?>
								</select>
								</p>
								<p><input class="btn-primary" type="submit" value="<?php _e('Next','mooncupmain'); ?>" /></p>
							</form>
							-->
             
			
                <p>
              <?php do_action('wcpbc_manual_country_selector'); ?>
              </p>
               <p>
                 <a href="<?php echo get_permalink(icl_object_id(3545,'page',false));?>" class="btn-primary"><?php _e('Next','mooncupmain'); ?></a>
              </p>
          
						</div>




	        		</div>


	        <?php
			// check if the repeater field has rows of data
			if( have_rows('slider_gallery') ):?>
			<div class="story-slider">

			    <?php while ( have_rows('slider_gallery') ) : the_row();?>

				<div class="team-giving-item container--lined">
					<div class="team-giving-item__content center">
						<h3>
							<?php
							the_sub_field('slider_title');
							?>
						</h3>
						<?php
							the_sub_field('slider_content');
						?>

					</div>
				</div>

			    <?php endwhile;?>
			</div>
			<?php

			else :

			    // no rows found

			endif;

			?>
	        	</article>




</section>

<?php get_footer(); ?>
