<?php
/**
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 * Template Name: Shop LP with all options
 */


/**
*
* Match geocode with category value
*/
get_header();
$country = new WC_Countries;
$countries = $country->get_allowed_countries();
$taxonomy_c = 'online-stockists';
$tax_terms = get_terms($taxonomy_c);
if(isset($_COOKIE['location'])):
    $location_iso = $_COOKIE['location'];
    foreach($countries as $key =>$value) {
        if($location_iso == $key):
        $stockists = $wpdb->get_results($wpdb->prepare("SELECT wp_store_locator.* FROM wp_store_locator WHERE sl_country = %s",$value));
            foreach ($tax_terms as $tax_term) {
                if($tax_term->name == $value):
                    $location_name = $value;
                    break;
                else:
                    $location_name = '';
                endif;
            }
        endif;
    }
else:

endif;
?>
<style>
	.shop-mooncup-all .product-listing ul.products li.product .price:after {
		display: inline-block;
		content: "(<?php _e('Free delivery','mooncupmain'); ?>)";
	}
</style>
<section class="shop-mooncup shop-mooncup-all single-col page-content primary" role="main">

	        <article class="container_full splash-content-block">
	        	<div class = "splash-image-narrow splash-image_generic image_fullwidth" style="background-image:url('<?php the_field('splash_image'); ?>');">
		        	<div class="splash-content-overlay splash-header text-reverse">
		        		<div class="container_full">
			        	<?php the_field('splash_content'); ?>
			        	</div>
		        	</div>
		        </div>
		    </article>

	        <article class="container_full">
	        	<div class="shop container_boxed">
	        		<div class="buy-online col__8">
	        			<div class="buy-online-title">
	        				<?php the_field('buy_online_title');?>
	        			</div>
	        			<div class="product-listing">
	        				<?php the_field('product_listing');?>
	        			</div>
	        		</div>

	        		<div class="buy-elsewhere col__4">
	        		<div class="find-stockist">
	        		<?php

	        		if(!empty($stockists)){?>
							<h2><?php _e('Find a stockist near you','mooncupmain');?></h2>
							<p class="center"><a class="btn-primary" href="/buy-the-mooncup/find-a-stockist/?addressInput=<?php if($location_name == 'United Kingdom (UK)'){ echo 'UK';}elseif($location_name == 'Republic of Ireland'){echo 'Ireland';}elseif($location_name == 'New Zealand'){echo 'NewZealand';}else{ echo $location_name;};?>"><?php _e('Find a stockist','woocommerce'); ?></a></p>
	        				<!--<?php the_field('find_stockist');?>
							<form method="get" action="/buy-the-mooncup/find-a-stockist/">
							<label class="blue-caps">Enter your location: <br />(City or Post Code)</label>
							<input type="text" id="addressInput" name="addressInput" placeholder="Enter your postcode/city" size="50" value="">
							<input type="submit" value="<?php _e('Find Stockist','mooncupmain'); ?>"/>
							</form>-->

	        		<?php
	        		}else{
	        		 echo "";
	        		};
	        		?>
	        		</div>
	        		<div class="find-online">
					<?php
					if($location_name != ''):?>

	        				<?php the_field('find_online');?>
							<form class="find-stockist" method="get" action="/buy-the-mooncup/find-an-online-retailer/">
								<label class="blue-caps"><?php _e('Select your country','mooncupmain'); ?>:</label>
								<select name="online-location">
									<?php
									$taxonomy = 'online-stockists';
									$tax_terms = get_terms($taxonomy);

									foreach ($tax_terms as $tax_term) {
									    if($tax_term->name == $location_name ):

									       echo '<option value="'.$tax_term->name.'" selected>'.$tax_term->name.'</option>';
									    else:
										  echo '<option value="'.$tax_term->name.'">'.$tax_term->name.'</option>';
									    endif;
									}
									?>
								</select>
								<input type="submit" value="<?php _e('Find Online','mooncupmain');?>"/>
							</form>

					<?php
					else:
					   echo "";
					endif;
					?>
					</div>
	        		</div>
	        	</div>
	        </article>

</section>

<?php get_footer(); ?>
