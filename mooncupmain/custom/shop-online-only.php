<?php
/**
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 * Template Name: Shop LP online only
 */

get_header(); ?>
<section class="shop-mooncup shop-mooncup-all single-col page-content primary" role="main">
		
	        <article class="container_full splash-content-block">
	        	<div class = "splash-image-narrow splash-image_generic image_fullwidth" style="background-image:url('<?php the_field('splash_image'); ?>');">
		        	<div class="splash-content-overlay splash-header text-reverse">
		        		<div class="container_full">
			        	<?php the_field('splash_content'); ?>
			        	</div>
		        	</div>
		        </div>
		    </article>

	        <article class="container_full">
	        	<div class="shop container_boxed">
	        		<div class="buy-online col__8">
	        			<div class="buy-online-title">
	        				<?php the_field('buy_online_title');?>
	        			</div>
	        			<div class="product-listing">
	        				<?php the_field('product_listing');?>
	        			</div>
	        		</div>

	        		<div class="buy-elsewhere col__4">
	        			<div class="additional-info">
	        				<?php the_field('additional_info');?>
	        			</div>
	        		</div>
	        	</div>
	        </article>
	
</section>

<?php get_footer(); ?>
